# Define the provider for AWS
provider "aws" {
  region = "us-east-1"  # Replace with your desired region
}

# Create an S3 bucket
resource "aws_s3_bucket" "opercredit_bucket" {
  bucket = "opercredit-bucket"  # Replace with your desired bucket name
  acl    = "private"

  tags = {
    Name = "opercredit Bucket"
  }
}

# Output the bucket name
output "bucket_name" {
  value = aws_s3_bucket.opercredit_bucket.id
}
