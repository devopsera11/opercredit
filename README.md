# devops_tasks:

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/devopsera11/opercredit.git
git branch -M main
git push -uf origin main
```


***

## Name
DevOps assessment

## Description
DevOps Task

Problem statement
The assessment includes challenges across the whole stack for DevOps-related tasks, in order to test skills in:
CI/CD
Kubernetes
Infrastructure as Code
Web server coding
We would like to assess how the candidate can switch between different tools and
technologies, while requiring them to think about delivering an end-to-end solution.
The candidate should use the technologies that are highlighted in
bold in the list below, as much as possible, but some alternatives can be used if
he/she has already lot of experience with them and no experience with our preferred
ones.
CI/CD → Gitlab CI - (Alternatives: Github Actions)
Coding → Python / NodeJS - (Alternatives: Go or Java)
Cloud Provider → AWS / GCP - (Alternatives: Azure)
Infrastructure as Code → Terraform
Kubernetes deployments → Helm / Kubectl / Terraform
The candidate should provide an sufficient documentation on how the proposed
submission solves the requirements and how the code can be used successfully on
our end.

Tasks:
1. Using Terraform, define the infrastructure that enables an application to
programmatically upload files to a cloud storage. (e.g. S3 Bucket, GCP Cloud
Storage, ...)

2. Develop a web server that exposes on the port 4545 the following endpoints:
a. GET /api/health returns a JSON payload containing { "status": "ok" }
b. POST /api/upload-random uploads a randomly-named file (content of the file
is not relevant) to the storage created before.

3. Using CI/CD, build a Docker image of the application and push it to a registry
any time there is a push on the repo.

4. Deploy the application on a K8s cluster (you can use MiniKube for spinning a
cluster locally) using helm / kubectl / terraform. The deliverable should:
Use the docker image you pushed to the registry
Provide an Ingress that listens on port 80 and redirect traffic to the
application


## Authors and acknowledgment
Dimitrios Sakellaropoulos, DevOps Engineer.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
