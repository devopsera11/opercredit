kubectl describe svc webserver-service

Name:              webserver-service
Namespace:         default
Labels:            <none>
Annotations:       <none>
Selector:          app=webserver
Type:              ClusterIP
IP Family Policy:  SingleStack
IP Families:       IPv4
IP:                10.104.24.248
IPs:               10.104.24.248
Port:              <unset>  4545/TCP
TargetPort:        4545/TCP
Endpoints:         10.244.0.47:4545
Session Affinity:  None
Events:            <none>